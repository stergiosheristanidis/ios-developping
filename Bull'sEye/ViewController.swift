//
//  ViewController.swift
//  Bull'sEye
//
//  Created by Ste Heri on 3/11/15.
//  Copyright (c) 2015 NBF LLC. All rights reserved.
//

import UIKit
import QuartzCore

class ViewController: UIViewController {
    var currentValue: Int!
    @IBOutlet weak var slider : UISlider!
    @IBOutlet weak var score : UILabel!
    @IBOutlet weak var round : UILabel!
    @IBOutlet weak var randomNumber : UILabel!
    
    var scoreSucceded: Int!
    var roundNow : Int!
    var succesiveBingos : Int!
    var targetValue : Int!
  
@IBAction func showAlert()
    {
        var title : String
        let alertMessage = "Το σκορ σου ήταν: \(currentValue)"
        let difference = abs(currentValue - targetValue)

        if difference == 0
        {
            title = "Τέλεια!!! Παίρνεις bonus 50 πόντους"
            succesiveBingos = succesiveBingos + 1
            scoreSucceded = scoreSucceded + 150 + succesiveBingos*50
        }
        else if difference < 5
        {
            title = "Πολύ κοντά"
            scoreSucceded = scoreSucceded + 150 - difference + 25
            succesiveBingos = 0
        }
        else if difference < 10
        {
            title = "Είσαι καλός"
            scoreSucceded = scoreSucceded + 150 - difference + 10
            succesiveBingos = 0
        }
        else
        {
            title = "Αστόχησες"
            scoreSucceded = scoreSucceded + 150 - difference
            succesiveBingos = 0
        }
let alert=UIAlertController(title: title, message: alertMessage ,preferredStyle: UIAlertControllerStyle.ActionSheet)
        //to alertController μπορεί να είναι είτε ActionSheet eite aplo Alert
        let action=UIAlertAction(title: "Μπράβο", style: UIAlertActionStyle.Cancel, handler: {action in self.roundStart() }
       )
        
        //sto action mporoume na baloume na kanei kati molis pathseis to ok, sto handler
        alert.addAction(action)
        
        presentViewController(alert, animated: true, completion: nil)
        
        
       
       
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let thumbImageSliderNormal = UIImage(named: "SliderThumb-Normal")
        slider.setThumbImage(thumbImageSliderNormal, forState: .Normal)
        
        let thumbImageSliderHighlited = UIImage(named: "SliderThumb-Highlited")
        slider.setThumbImage(thumbImageSliderHighlited, forState: UIControlState.Highlighted)
        
        let insets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
        
        if let trackLeftImage = UIImage(named: "SliderTrackLeft")
        {
            let trackLeftResizable=trackLeftImage.resizableImageWithCapInsets(insets)
            slider.setMinimumTrackImage(trackLeftImage, forState: UIControlState.Normal)
            
        }
        else
        {
            let trackRightImage = UIImage(named: "SliderTrackRight")
            slider.setMaximumTrackImage(trackRightImage, forState: UIControlState.Normal)
        }
        
        slider.frame.height .advancedBy(40.0)
        // Do any additional setup after loading the view, typically from a nib.
        currentValue=lroundf(slider.value)
       startNewGame()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func sliderMoved(slider: UISlider)
{
    currentValue=lroundf(slider.value)
    }
func roundStart()
{
    roundNow = roundNow + 1
    slider.value = Float(75)
    //(1 + Int(arc4random_uniform(150) dhmiourgei enan tuxaio (pseudo) arithmo apo to 1 mexri to 150
   updateLabels()
    }
    func updateLabels()
    {
        targetValue = 1 + Int(arc4random_uniform(150))
    randomNumber.text = String(targetValue)
    score.text=String(scoreSucceded)
    round.text=String(roundNow)
    }
  @IBAction func startNewGame()
    {
        let transition = CATransition()
        transition.type = kCATransitionFade
        transition.duration = 1
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        
        view.layer.addAnimation(transition, forKey: nil)
        
        
        
        succesiveBingos = 0
        roundNow = 0
        scoreSucceded = 0
        roundStart()
        
    }
}

